<?php

require_once 'functions.php';

//1. rozsekání "hezké URL" na požadované částí$currentUrl = getCurrentURL();
$url = str_replace("http://localhost/~ela/", "", getCurrentURL());
$url = explode('/',$url."////");

//2. lepe:
$purl = parse_url($_SERVER['REQUEST_URI']);
print_r($purl);//vypis pro kontrolu
//odstranime $base: "instalacni" adresar v apache
$url = str_replace($base, "", $purl['path']);

//dvojita lomitka:
$url = str_replace('//', '/', $url . '/default/default/default/default');
print_r('url=' . $url);
$path = explode('/', $url);

print_r($path);

//tady mapujeme podle v1,2,3,4 slozek z path URI na dynamicke stranky
// - viz take obsah .htaccess,
//pokud mapovani neexistuje mapujeme na pages/default.php!

switch ($path[1]) {
 case 'about': require_once 'pages/aboutMe.php';
 case 'dyn0':
    switch ($path[2]) {
    case 'dyn1': require_once 'pages/dyn1/dyn1.php';
    default:     require_once 'pages/dyn1/default.php';   
    }
 default:
     require_once 'pages/default.php';
}
